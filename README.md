## King Pong

Welcome to King Pong! A game developed for Ludum Dare 33, written in Python / Pygame.

Play Pong the way all giant apes do, by bouncing a car back and forth.

The controls:

    Player 1 - W / S
    Player 2 - Up / Down

With six hours left to go, I realised I had a silly idea that might be simple enought to complete in time for submission. Plus, I got to try out my new, bottom-of-the-range graphics tablet.

------------------------------------------------------------------

### Reviews:

* ["not *too* terrible" - Jupiter Hadley](https://www.youtube.com/watch?v=NYc-eDKmAKE)

------------------------------------------------------------------

### Installation:

This game is available as a stand-alone Windows 64-bit executable, and also as Python source.

The Python source should run on any platform. Just install Python, Pygame, and then run game.py
