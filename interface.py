#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import pygame

from defines import *
from resource import res


class InterfaceItem(object):
    """Base Interface class, for subclassing"""
    def __init__(self, surf, rect):
        self.surf = surf
        self.rect = rect
        interface_list.append(self)
        self.dirty = False

    def draw(self):
        pygame.draw.rect(self.surf, (0,0,0), self.rect, 0)
        self.dirty = False


# helper functions
def draw_text(text, surface, rect, font_filename=None, size=10, color=(255,255,255), antialias=True, background=None):
    """Draw a line or paragraph of text, wrapping at a supplied pixel width"""

    if text == "" or text is None:
        return

    # prepare the font
    if font_filename is None:
        font_filename = DEFAULT_TEXTBLOCK_FONT

    font = pygame.font.Font(font_filename, size)

    # prepare the text
    rendered_lines = []
    lines = text.split("\n")
    width = rect.width

    debug("Wrapping text to %d pixels" % width)

    for line in lines:
        words = line.split(" ")
        line_words = []
        line_text = ""
        
        debug("Parsing line: '%s'" % line)
                
        # keep adding words until you hit the line length
        for index in range(len(words)):
            word = words[index]

            debug("Adding word '%s' to line" % word)
            
            line_words.append(word)
            line_text = " ".join(line_words)

            line_size = font.size(line_text)[0]
            debug("Line is now %d / %d pixels" % (line_size, width))

            if line_size > width:
                # too many words!
                line_text = " ".join(line_words[:-1])
                if background is None:
                    rendered_lines.append(font.render(line_text, antialias, color))
                else:
                    rendered_lines.append(font.render(line_text, antialias, color, background))
                line_words = [word]
                debug("Next line now has word '%s'" % word)
                                     
        # make sure all words in this line have been rendered:
        if len(line_words) != 0:
            line_text = " ".join(line_words)
            if background is None:
                rendered_lines.append(font.render(line_text, antialias, color))
            else:
                rendered_lines.append(font.render(line_text, antialias, color, background))

    # draw the paragraph!
    x, y = rect.topleft
    for line_surface in rendered_lines:
        surface.blit(line_surface, (x, y))
        y += font.get_height()


def iconify(surf, width=32, height=32):
    """Return a new surface with the image resized down suitable for an icon.

    The image is scaled without distorting its proportions.
    """

    x, y = surf.get_size()
    src_ratio = x / y
    dest_ratio = width / height

    # scale
    if src_ratio == dest_ratio:
        return pygame.transform.smoothscale(surf, (width, height))
    else:
        if src_ratio > dest_ratio:
            target_size = (width, int(width / src_ratio))
        else:
            target_size = (int(height * src_ratio), height)
        new_surf = pygame.transform.smoothscale(surf, target_size)

        # paste onto a surface of the desired size
        retval = pygame.Surface((width, height))
        rect = new_surf.get_rect(center=retval.get_rect().center)

        retval.blit(new_surf, rect)
        return retval


# main
interface_list = []

if __name__ == "__main__":
    import game
    game.main()
