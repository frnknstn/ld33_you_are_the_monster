#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import random
import math

import pygame

from defines import *

import sprite_types

class PlayerMob(sprite_types.Mob):
    def __init__(self, pos, direction="none", sprites = None):
        sprite_types.Mob.__init__(self, "player", pos, direction)

        self.child_sprites = []

        # assume the propulsion can offset friction
        self.mu_coefficient = 0
        self.solid = True

        # input flags - player control
        self.accelerating = False
        self.stopping = False
        self.steer_left = False
        self.steer_right = False
        self.gun = False

        # physical location of weapons on sprite
        self.gun_location = (0, 0)

        # image rotation-related
        self.image_real = self.image
        self.image_heading = 0

        self.spawn()


    def spawn(self):
        """Reset our variables to default"""
        self.dead = False
        self.set_move_delta(0, 0)

        # player-specific
        self.engine_speed = 0
        self.acceleration = 0.0139
        self.braking = 0.0150
        self.max_speed = 3
        self.hitpoints = 100

        # keep our degree heading separate, as the sprite can go upside down
        self.heading = 0
        self.turn_rate = 2

        # weapon-related
        self.gun_cooldown = 0
        self.gun_delay = 10

        # animation
        self.start_anim(self.anim_idle())


    def get_rotated_pos(self, x, y):
        """Converts an offset on the base image to approx. coordinates on the rotated image

        Calculation done relative to the center of the image.

        Returns the (x, y)"""

        # convert to polar
        theta = math.atan2(y, x)
        r = math.hypot(x, y)

        # rotate
        theta += self.heading * math.pi / 180

        # convert back to cartesian
        x = r * math.cos(theta)
        y = -r * math.sin(theta)
        return (x, y)

    def handle_input(self, event):
        """Update our state base on all the keyboard input.

        parse_input() uses these flags to actually do the changes.

        Returns True if we consumed the event, False if we were unable to handle it.
        """

        handled = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self.accelerating = True
            elif event.key == pygame.K_DOWN:
                self.stopping = True
            elif event.key == pygame.K_RIGHT:
                self.steer_right = True
            elif event.key == pygame.K_LEFT:
                self.steer_left = True
            elif event.key == pygame.K_SPACE:
                self.gun = True
            elif event.key == pygame.K_v:
                # alias for spacebar
                self.gun = True
            else:
                handled = False

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP:
                self.accelerating = False
            elif event.key == pygame.K_DOWN:
                self.stopping = False
            elif event.key == pygame.K_RIGHT:
                self.steer_right = False
            elif event.key == pygame.K_LEFT:
                self.steer_left = False
            elif event.key == pygame.K_SPACE:
                self.gun = False
            elif event.key == pygame.K_v:
                self.gun = False
            else:
                handled = False

        else:
            handled = False

        return handled

    def parse_input(self):
        """update the object based on the input"""

        if self.dead:
            return

        update_needed = False

        # speed
        if self.accelerating:
            self.engine_speed += self.acceleration
            if self.engine_speed > self.max_speed:
                self.engine_speed = self.max_speed
            update_needed = True

        if self.stopping:
            self.engine_speed -= self.braking
            if self.engine_speed < 0:
                self.engine_speed = 0
            update_needed = True

        # heading
        if self.steer_left:
            self.heading += self.turn_rate
            update_needed = True
        if self.steer_right:
            self.heading -= self.turn_rate
            update_needed = True

        if self.heading > 180:
            self.heading -= 360
        elif self.heading <= -180:
            self.heading += 360

        # update the actual physics
        if update_needed:
            self.set_move_vector(self.heading * math.pi / -180, self.engine_speed)

    def fire_weapons(self, sprites, frame_count):
        """Process weapons fire. Can add new sprites."""
        if self.gun and (self.gun_cooldown <= frame_count):
            self.gun_cooldown = frame_count + self.gun_delay

            # locate the physical position of the weapon
            pos = self.pos
            pos_offset = self.get_rotated_pos(*self.gun_location)
            pos = (pos[0] + pos_offset[0], pos[1] + pos_offset[1])

            sprites.append(sprite_types.BulletMob(pos, self.theta, self.dr, self))

    def herd_children(self, sprites):
        """Move our child sprites into the main sprite list"""
        sprites.extend(self.child_sprites)
        self.child_sprites = []

    def anim_rotate(self, force = False):
        """Rotate the canonical image to produce one suitable for our heading

        Returns True if the image changed

        Called from the animation coroutines
        """
        if (not force) and abs(self.heading - self.image_heading) < 2:
            # ignore small changes since this is quite an expensive operation
            return False

        tmp_image = self.image_real

        self.image = pygame.transform.rotate(tmp_image, self.heading)
        self.image_heading = self.heading

        # maintain the center of our rect
        center = self.rect.center
        self.rect.size = self.image.get_size()
        self.rect.center = center

        return True

    def anim_move(self):
        """Animation coroutine for move animation"""
        frames = self.get_frames("idle")
        force_rotate = True
        image_changed = True

        while True:
            for frame in frames:
                for i in range(15):
                    if (i == 0) and (frame != self.image_real):
                        self.image = frame
                        self.image_real = frame
                        force_rotate = True

                    image_changed = self.anim_rotate(force = force_rotate)
                    force_rotate = False

                    if self.engine_speed == 0:
                        # full stop!
                        self.start_anim(self.anim_idle())
                        image_changed = True

                    yield image_changed

    def anim_idle(self):
        """Animation coroutine for idle animation"""
        frames = self.get_frames("idle")
        force_rotate = True
        image_changed = True

        while True:
            for frame in frames:
                for i in range(15):
                    if (i == 0) and (frame != self.image_real):
                        self.image = frame
                        self.image_real = frame
                        force_rotate = True

                    image_changed = self.anim_rotate(force = force_rotate)
                    force_rotate = False

                    if self.engine_speed > 0:
                        # start your engines!
                        self.start_anim(self.anim_move())
                        image_changed = True

                    yield image_changed


if __name__ == "__main__":
    import game
    game.main()
