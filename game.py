#!/usr/bin/python
# coding=utf-8

"""Ludum Dare Base Code

"""

from __future__ import division
from __future__ import print_function

import os
import sys
import math
import random
import time
import itertools
from math import sqrt, pow

import pygame
if False:
    # a hint for cx_freeze
    import pygame._view

from defines import *
from level import Level
from viewport import Viewport
import sprite_types
import player_mob
import resource
import interface

class FramerateCounter(object):
    buffer_size = 60
    
    def __init__(self):
        self.rates = [0 for i in range(self.buffer_size)]
        self.average = 0.0
        self.last_time = time.clock()
        self.index = 0
        self.zero_interval_frames = 0
        self.font = pygame_default_font(16)

    def frame(self):
        """signal the next frame to the FPS counter, and return the current average framerate"""
        if sys.platform.startswith('linux'):
            this_time = time.time()
        else:
            this_time = time.clock()
        this_interval = (this_time - self.last_time)

        if this_interval == 0:
            # Too short a time between frames, or insufficient timer resolution
            # Don't log this frame
            self.zero_interval_frames += 1
            return self.average

        this_rate = (1 + self.zero_interval_frames) / (this_time - self.last_time)
        self.zero_interval_frames = 0

        # circular buffer for frame rates
        self.index += 1
        if self.index >= self.buffer_size:
            self.index = 0
        self.rates[self.index] = this_rate

        self.last_time = this_time
        self.average = sum(self.rates) / self.buffer_size

        return self.average

    def draw(self, surf):
        surf.blit(self.font.render("FPS: %0.3f" % self.average, True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))


class Game(object):
    def __init__(self):
        self.set_state("config")
        self.player_score = 0
        self.oblivion_score = 0

        self.level = 1

        self.sfx = {}

        self.running = True

    def main(self):
        global frame_count

        debug("Game.main() running")

        # Play music loop
        pygame.mixer.music.load("music/bu-feet-of-trolls.ogg")
        pygame.mixer.music.play(-1)

        # allow the user to select double size mode
        if self.state == "config":
            global DOUBLE_SIZE
            DOUBLE_SIZE = False
            screen = pygame.display.set_mode(SCREEN_SIZE)
            self.config_screen(screen)
            self.set_state("main")

        # set up double-size, if needed
        if not DOUBLE_SIZE:
            screen = pygame.display.set_mode(SCREEN_SIZE)
        else:
            self.real_screen = pygame.display.set_mode(
                (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2))
            screen = pygame.Surface(SCREEN_SIZE)
        self.screen = screen

        self.clock = pygame.time.Clock()

        sprite_types.load_sprite_files()

        # sound resources
        self.sfx["hit1"] = pygame.mixer.Sound('sound/hit1.wav')
        self.sfx["hit2"] = pygame.mixer.Sound('sound/hit2.wav')
        self.sfx["score"] = pygame.mixer.Sound('sound/score.wav')
        self.sfx["bounce"] = pygame.mixer.Sound('sound/bounce.wav')

        self.running = True
        self.set_state("main")

        while self.running:
            if self.state == "main":
                debug("level number %d" % self.level)
                # set up level

                # start the level
                self.main_game(screen)
                self.level += 1

            elif self.state == "scores":
                # show a different screen
                pass

        debug("Leaving main loop")
        screen.fill((0, 0, 0))

        self.flip()

    def main_game(self, screen):
        """Run the main game"""
        players = []
        sprites = []        # all sprites and things, including the players

        pygame.INFO_SPRITE_COUNT = 0

        # prepare the stuff
        player1 = sprite_types.MonkeyMob((LEFT_BASELINE, MIDLINE[1]), "right")
        players.append(player1)
        sprites.append(player1)

        player2 = sprite_types.MonkeyMob((RIGHT_BASELINE, MIDLINE[1]), "left")
        players.append(player2)
        sprites.append(player2)

        ball = sprite_types.BallMob(MIDLINE, "right")
        sprites.append(ball)
        score_delay_remaining = SCORE_DELAY

        # populate the level
        map_image = pygame.image.load("map/map.jpg").convert()
        screen.blit(map_image, (0,0))

        # prepare the GUI
        score_font = pygame.font.Font(DEFAULT_TEXTBLOCK_FONT, 64)

        self.flip()

        # prepare the loop
        frame_count = 0
        fps = FramerateCounter()
        info_font = pygame_default_font(16)

        # Main Loop
        self.set_state("main")

        while self.running:
            # parse events and input
            self.parse_events(handlers=(x.handle_input for x in players))

            for player in players:
                player.parse_input()

            if self.ai:
                player2.handle_ai(ball)

            # activate the ball after a delay
            if score_delay_remaining > 0:
                score_delay_remaining -= 1
                if score_delay_remaining == 0:
                    ball.activate("right")

            # Update all the sprites
            for sprite in sprites:
                sprite.update_animation()

            # Remove dead sprites
            old_sprites_count = len(sprites)
            sprites = [ x for x in sprites if not x.dead ]
            if len(sprites) != old_sprites_count:
                debug("Removed %d dead sprites" % (old_sprites_count - len(sprites)))

            # add all new sprites to the master sprite list
            for player in players:
                player.herd_children(sprites)

            self.move_players(players)
            self.move_ball(ball, players)

            # did someone score?
            scored = False
            if ball.pos[0] < LEFT_EDGE:
                scored = True
                for player in players:
                    if player.direction == "left":
                        player.score += 1
                        break
            elif ball.pos[0] > RIGHT_EDGE:
                scored = True
                for player in players:
                    if player.direction == "right":
                        player.score += 1
                        break

            if scored:
                self.sfx["score"].play()

                # make a new ball
                ball.dead = True
                ball = sprite_types.BallMob(MIDLINE, "right")
                sprites.append(ball)
                score_delay_remaining = SCORE_DELAY

            # draw screen
            screen.blit(map_image, (0,0))

            # Draw our sprites:
            if sprites is not None:
                for sprite in sprites:
                    pygame.INFO_SPRITE_COUNT += 1
                    screen.blit(sprite.image, sprite.rect)


            # draw score
            left_score = right_score = 0
            for player in players:
                if player.direction == "right":
                    right_score += player.score
                else:
                    left_score += player.score

            screen.blit(score_font.render(str(left_score), True, (220,210,15)), LEFT_SCORE_POS)
            screen.blit(score_font.render(str(right_score), True, (220,210,15)), RIGHT_SCORE_POS)

            # draw interfaces
            for item in interface.interface_list:
                item.draw()

            frame_count += 1
            fps.frame()

            if DEBUG:
                # draw debug info
                fps.draw(screen)
                info = "%dx%d window, %d viewports, %d sprites (%d drawn), %d frames" % (
                    SCREEN_SIZE[0], SCREEN_SIZE[1], 0, len(sprites), pygame.INFO_SPRITE_COUNT,
                    frame_count
                    )
                screen.blit(info_font.render(info, True, (255,255,255), (0,0,0)), (0, 12))
                pygame.INFO_SPRITE_COUNT = 0

            self.flip()

            if self.state != "main":
                # leave the main loop
                break

        # exit main game loop func


    def flip(self, surface=None):
        """Move the screen surface to the actual display"""
        if surface is None:
            surface = self.screen

        if DOUBLE_SIZE:
            pygame.transform.scale(surface, (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2), self.real_screen)

        pygame.display.flip()

        if CAP_FPS is not None:
            self.clock.tick(CAP_FPS)
        else:
            # be a good multitasking program buddy
            time.sleep(0.0005)

    def move_ball(self, ball, players):
        # apply physics

        # spin the ball
        if ball.spin != 0:
            heading = ball.heading + ball.spin
            heading = heading

            if heading > 180:
                heading -= 360
            elif heading <= -180:
                heading += 360

            ball.heading = heading

        # bounce the ball
        x, y = ball.pos
        dx, dy = ball.dx, ball.dy
        if y <= TOP_EDGE:
            dy *= -1
            self.sfx["bounce"].play()
        elif y >= BOTTOM_EDGE:
            dy *= -1
            self.sfx["bounce"].play()

        # bats
        hit = False
        for player in players:
            if ball.rect.colliderect(player.paddle.rect):
                if player.direction == "right" and dx < 0:
                    hit = True
                    hit_theta_dir = -1
                elif player.direction == "left" and dx > 0:
                    hit = True
                    hit_theta_dir = 1

                if hit:
                    dx *= -1
                    self.sfx[random.choice(("hit1", "hit2"))].play()

                    # adjust / speed angle of bounce based off distance from centre of the paddle
                    hit_theta = (player.paddle.pos[1] - ball.pos[1]) * 1.5 / 100 * hit_theta_dir
                    hit_dr = 0.25
                    print("dist", player.paddle.pos[1] - ball.pos[1], "theta", hit_theta)
                    break

        ball.set_move_delta(dx, dy)
        if hit:
            ball.set_move_vector(ball.theta + hit_theta, ball.dr + hit_dr)

        # apply physics - air resistance
        # TODO: Apply to spin only
        # if dr != 0 and ball.mu_coefficient != 0:
        #     # faster objects have more turbulence
        #     speed_scale = (dr / RESISTANCE_SCALE)
        #
        #     dr -= AIR_RESISTANCE * ball.mu_coefficient * speed_scale


        # limit ball's theta to reasonable values
        theta = ball.theta
        if abs(theta) < math.pi / 2:
            if abs(ball.theta) > BALL_MAX_THETA:
                if ball.theta > 0:
                    good_theta = BALL_MAX_THETA
                else:
                    good_theta = -BALL_MAX_THETA
                ball.set_move_vector(good_theta, ball.dr)
        elif abs(theta) > math.pi / 2:
            if abs(ball.theta) < (math.pi - BALL_MAX_THETA):
                if ball.theta > 0:
                    good_theta = (math.pi - BALL_MAX_THETA)
                else:
                    good_theta = -(math.pi - BALL_MAX_THETA)
                ball.set_move_vector(good_theta, ball.dr)

        ball.move(None)


    def move_players(self, players):
        for sprite in players:
            sprite.move(None)
            sprite.paddle.move_to(sprite.paddle.pos[0], sprite.pos[1])

    def move_sprites(self, sprites, level):

        for sprite in sprites:
            if not isinstance(sprite, sprite_types.Mob):
                continue

            # apply physics
            if sprite.spin != 0:
                heading = sprite.heading
                heading = heading + sprite.spin

                if heading > 180:
                    heading -= 360
                elif heading <= -180:
                    heading += 360

                sprite.heading = heading

            # apply physics - air resistance
            # TODO: Apply to spin only
            dr = sprite.dr

            if dr != 0 and sprite.mu_coefficient != 0:
                # faster objects have more turbulence
                speed_scale = (dr / RESISTANCE_SCALE)

                dr -= AIR_RESISTANCE * sprite.mu_coefficient * speed_scale

                sprite.set_move_vector(sprite.theta, dr)

            sprite.move(level)

    def set_state(self, state):
        assert(state in GAME_STATES)
        debug("Game state changed to '%s'" % state)
        self.state = state

    def parse_events(self, handler=None, handlers=None):
        """Parse the PyGame event queue, handling global keys, or passing them off to a handler otherwise"""

        def dummy_handler(event):
            """dummy event handler"""
            return False

        def try_handlers(event):
            """Try the supplied handlers in order"""
            for handler in handlers:
                handled = handler(event)
                if handled:
                    return True
            return False

        # default parameters
        if handler is None:
            handler = dummy_handler

        if handlers is None:
            handlers = (handler,)

        # event loop
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_ESCAPE, pygame.K_q):
                    self.running = False
                elif not try_handlers(event):
                    debug(event)

            elif event.type == pygame.KEYUP:
                try_handlers(event)

            elif event.type == pygame.QUIT:
                self.running = False

    def config_screen(self, screen):
        """config options"""
        # handler to allow player to exit the screen
        def handler(event):
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    self.ai = True
                    self.skip_ahead = True
                elif event.key == pygame.K_2:
                    self.ai = False
                    self.skip_ahead = True
        self.skip_ahead = False

        # draw the story
        screen.fill((0,0,0))
        map_image = pygame.image.load("map/map.jpg").convert()
        screen.blit(map_image, (0,0))
        header = pygame_default_font(52).render("KING PONG", True, (255,0,0), (0,0,0))
        screen.blit(header, (64, 64))

        story = """
Press 1 for single player vs. terrible AI
Press 2 for two-player

Controls:
PLAYER 1 - W / S
PLAYER 2 - Up / Down


"""

        interface.draw_text(story.upper(), screen, pygame.Rect(64, 128, SCREEN_SIZE[0] - 64*2, SCREEN_SIZE[1]), size=24)

        footer_text = "(press 1 or 2, or Q to quit)"
        footer = pygame_default_font(22).render(footer_text, True, (185,185,185), (0,0,0))

        # center the footer
        footer_x = 0.5 * (SCREEN_SIZE[0] - footer.get_width())
        screen.blit(footer, (footer_x, SCREEN_SIZE[1] - 24))

        # can't use the standard stuff this early
        pygame.display.flip()

        while self.running and not self.skip_ahead:
            self.parse_events(handler)
            time.sleep(0.01)


    def flash_screen(self, screen, next_state):
        """Draw a whole-screen flash transition"""
        flash_surface = pygame.Surface(SCREEN_SIZE)
        flash_surface.fill((255,255,255))

        back_buffer = pygame.Surface(SCREEN_SIZE)
        back_buffer.blit(screen, (0,0))

        frames = 0
        flash_duration = 40

        while self.running:
            self.parse_events()

            screen.blit(back_buffer, (0,0))

            flash_surface.set_alpha(int(frames / flash_duration * 255))
            screen.blit(flash_surface, (0,0))

            self.flip()

            frames += 1
            if frames > flash_duration:
                break

        self.set_state(next_state)


class ExampleInterface(interface.InterfaceItem):
    def __init__(self, surf, rect):
        interface.InterfaceItem.__init__(self, surf, rect)

        self.font = pygame_default_font(18)
        self.title = self.font.render("Bla Bla:", True, (255,255,255))

        self.bar_graph = pygame.Surface((16, 8))
        self.bar_graph.fill((255,0,0))

        self.bar_value = 0.0

        self.bar_graph = pygame.Surface((16, 8))
        self.bar_graph.fill((255,255,0))

    def draw(self):
        bar_color = max(0, 255 - int(self.bar_value * 255))
        self.bar_graph.fill((255, bar_color, 0))

        origin = self.rect.topleft
        self.surf.blit(self.title, origin)
        self.surf.blit(self.bar_graph, (origin[0], origin[1] + 16))


def main():
    try:
        # reduced buffer size for lower audio latency
        pygame.mixer.pre_init(buffer=256)
        pygame.init()

        resource.load_resources()

        game = Game()
        game.main()

    finally:
        debug("Stopping pygame...")
        pygame.quit()

    debug("Exiting.")


if __name__ == "__main__":
    main()
