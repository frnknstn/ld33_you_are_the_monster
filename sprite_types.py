#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import os
import collections
import random
import math

import pygame

from defines import *

class MissingFramesException(Exception):
    pass

def load_sprite_files():
    """Load sprites from the disk which match the expected format.

    format is spritename_frameset_direction_index.png
    """
    filenames = os.listdir(SPRITE_DIR)
    filenames.sort()

    image_cache = collections.defaultdict(lambda : collections.defaultdict(dict))

    for filename in filenames:
        filepath = os.path.join(SPRITE_DIR, filename)
        if not os.path.isfile(filepath):
            continue

        # check the filename matches our expected format
        try:
            file_spritename, file_frameset_name, file_direction, file_index = os.path.splitext(filename)[0].split("_")
        except ValueError:
            debug("Ignoring misnamed file in sprite directory, '%s'" % filename)
            continue

        file_index = int(file_index)
        debug("Loading sprite '%s': '%s'-type's %s-facing frame %d of animation %s" % \
              (filename, file_spritename, file_direction, file_index, file_frameset_name))

        # load the sprite
        surf = pygame.image.load(filepath)
        surf.convert()

        # store in our cache
        key = (file_frameset_name, file_direction)
        image_cache[file_spritename][key][file_index] = surf


    # if there are no left frames for a right image, use a flipped right image
    for spritename, cache in image_cache.items():
        for key, frameset in cache.items():
            frameset_name, direction = key
            
            if direction != "right" or (frameset_name, "left") in cache:
                continue

            debug("Flipping %s's right-facing %s animation" % (spritename, frameset_name))
            flipped_frameset = {}
            for index, image in frameset.items():
                flipped_frameset[index] = pygame.transform.flip(image, True, False)

            cache[(frameset_name, "left")] = flipped_frameset

    # store the frames in the main cache
    for spritename in image_cache:
        Sprite.image_cache[spritename] = {}
        for key, value in image_cache[spritename].items():
            Sprite.image_cache[spritename][key] = [ value[x] for x in sorted(value) ]


class Sprite(object):
    """Base class for everything that has a sprite"""
    image_cache = {}

    def __init__(self, spritename, pos):
        # set up our animations
        self.anim = None        # our current animation coroutine
        self.image = None
        self.frame_sets = self.image_cache[spritename]

        self.spritename = spritename    # filename prefix used to load sprites

        # sample any of our sprites to determine our size
        if len(self.frame_sets) == 0:
            size =  (TILE_SIZE, TILE_SIZE)
        else:
            size = self.frame_sets.values()[0][0].get_size()

        # set up our positions
        self.pos = pos                                                          # center of the object
        self.tile_pos = (self.pos[0] // TILE_SIZE, self.pos[1] // TILE_SIZE)    # index of the tile we are on
        self.rect = pygame.Rect((0,0), size)                                    # actual sprite's draw rect
        self.rect.center = pos

        self.dead = False       # remove me from sprite lists now
        self.solid = False      # do colision detection


    def update_animation(self):
        """update our animation"""
        if self.anim is None:
            return False

        try:
            retval = self.anim.next()
        except StopIteration:
            debug("Animation on %s ran out!" % repr(self))
            self.anim = None
            retval = False

        return retval


    def get_frames(self, animation_name):
        """Get the most suitable set of frames for the given animation"""
        direction = self.direction

        # find a direction we have images for
        direction_found = True
        if (animation_name, direction) not in self.frame_sets:
            direction_found = False
            for priority in ("none", "right", "up", "down", "left"):
                if (animation_name, priority) in self.frame_sets:
                    direction = priority
                    direction_found = True
                    break

        # default to get the idle frames if we can't find any here
        if not direction_found:
            if animation_name != "idle":
                try:
                    retval = self.get_frames("idle")
                except MissingFramesException:
                    # raise exception below
                    pass
                else:
                    return retval
            raise MissingFramesException("Can't find any '%s' frames for %s" % (animation_name, str(self)))

        return self.frame_sets[(animation_name, direction)]

    def start_anim(self, anim):
        """Start playing an animation"""
        self.anim = anim
        return anim.next()

    def anim_stand(self):
        """Animation coroutine for standing animation"""
        frames = self.get_frames("stand")
        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(60):
                    yield False

    def __repr__(self):
        if self.anim is None:
            animation = "None"
        else:
            animation = self.anim.__name__
        return "<%s: %s, 0x%X>" % (self.__class__.__name__, animation, id(self))


class Mob(Sprite):
    """Base class for all mobile sprites"""
    def __init__(self, spritename, pos, direction="none"):
        Sprite.__init__(self, spritename, pos)

        self.hitpoints = 100
        
        self.dx = self.dy = 0.0         # X and Y velocity in pixels / frame
        self.dr = self.theta = 0.0      # velocity vector in pixels / frame

        self.mu_coefficient = 1.0       # friction multiplier for the object

        self.set_direction(direction)   # the facing of this sprite

    def damage(self, damage, source=None):
        """Damage this object, and possibly kill it"""
        if self.dead:
            # can't hurt a dead man
            return

        self.hitpoints -= damage

        if self.hitpoints <= 0:
            self.dead = True

        debug("%s took %0.3f damage from %s. HP now %0.3f" % (str(self), damage, str(source), self.hitpoints))

    def set_move_vector(self, theta, dr):
        """change the mob's velocity vector, as polar coords in pixels / frame"""
        self.theta = theta
        self.dr = dr

        self.dx = dr * math.cos(theta)
        self.dy = (dr * math.sin(theta))

    def set_move_delta(self, dx, dy):
        """change the mob's velocity, as cartesian coords in pixels / frame"""
        self.dx = dx
        self.dy = dy

        self.theta = math.atan2(dy, dx)
        self.dr = math.hypot(dx, dy)

    def set_direction(self, direction):
        assert direction in DIRECTIONS

        self.direction = direction
        self.start_anim(self.anim_walk())

    def move(self, level):
        """Move us one frame's worth"""

        # move us
        if self.dx == 0 and self.dy == 0:
            return

        old_x, old_y = self.pos
        x, y = old_x + self.dx, old_y + self.dy

        if BENCHMARK:
            # in benchmark mode, bounce the mobs of the edges of the map
            if x <= 0 or x >= level.size[0]:
                self.dx = -self.dx
            if y <= 0 or y >= level.size[1]:
                self.dy = -self.dy

        # do the move
        self.move_to(x,y)

    def move_to(self, x, y):
        """Move us to the specified coordinates"""
        self.pos = (x, y)
        self.rect.center = (round(x), round(y))

    def anim_walk(self):
        """Animation coroutine for walking animation"""
        frames = self.get_frames("walk")
        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(12):
                    yield False


class PaddleMob(Mob):
    def __init__(self, pos, direction="none"):
        Mob.__init__(self, "paddle", pos, direction)
        self.start_anim(self.anim_walk())


class MonkeyMob(Mob):
    def __init__(self, pos, direction):
        """A King Kong, or perhaps merely a Prince Kong?"""
        Mob.__init__(self, "monkey", pos, direction)
        self.child_sprites = []

        self.solid = True

        self.score = 0

        # movement input
        self.move_up = False
        self.move_down = False

        # disable physics
        self.spin = 0
        self.mu_coefficient = 0

        # paddle
        if self.direction == "right":
            self.paddle = PaddleMob((pos[0] + 35, pos[1]))
            self.handle_input = self.handle_input_right
        else:
            self.paddle = PaddleMob((pos[0] - 35, pos[1]))
            self.handle_input = self.handle_input_left

        self.child_sprites.append(self.paddle)

        self.start_anim(self.anim_walk())

    def handle_input_right(self, event):
        """Update our state based on all the keyboard input.

        parse_input() uses these flags to actually do the changes.

        Returns True if we consumed the event, False if we were unable to handle it.
        """

        handled = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                self.move_up = True
            elif event.key == pygame.K_s:
                self.move_down = True
            else:
                handled = False

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                self.move_up = False
            elif event.key == pygame.K_s:
                self.move_down = False
            else:
                handled = False

        else:
            handled = False

        return handled

    def handle_input_left(self, event):
        """Update our state based on all the keyboard input.

        parse_input() uses these flags to actually do the changes.

        Returns True if we consumed the event, False if we were unable to handle it.
        """

        handled = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self.move_up = True
            elif event.key == pygame.K_DOWN:
                self.move_down = True
            else:
                handled = False

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP:
                self.move_up = False
            elif event.key == pygame.K_DOWN:
                self.move_down = False
            else:
                handled = False

        else:
            handled = False

        return handled

    def handle_ai(self, ball):
        """Update our state based on all the the machinations of an unknowable AI.

        parse_input() uses these flags to actually do the changes.
        """
        # we may be close enough
        if abs(ball.pos[1] - self.pos[1]) < 65:
            if random.choice((True, True, False)):
                self.move_down = False
                self.move_up = False

        # do we need to move?
        if ball.pos[1] > self.pos[1]:
            self.move_down = True
        else:
            self.move_down = False

        if ball.pos[1] < self.pos[1]:
            self.move_up = True
        else:
            self.move_up = False

    def parse_input(self):
        """update the object based on the input"""

        if self.dead:
            return

        update_needed = False

        # parse the flags
        dy = self.dy
        if self.move_up and self.pos[1] > TOP_EDGE:
            dy -= MONKEY_ACCELERATION
            update_needed = True
        elif self.move_down and self.pos[1] < BOTTOM_EDGE:
            dy += MONKEY_ACCELERATION
            update_needed = True
        elif dy != 0:
            # slow down, monkey!
            if abs(dy) <= MONKEY_ACCELERATION:
                dy = 0
            elif dy < 0:
                dy += MONKEY_ACCELERATION
            else:
                dy -= MONKEY_ACCELERATION
            update_needed = True

        # change our velocity etc

        # update the actual physics
        if update_needed:
            dy = min(dy, MONKEY_MAX_DY)
            dy = max(dy, -MONKEY_MAX_DY)
            self.set_move_delta(self.dx, dy)

    def herd_children(self, sprites):
        """Move our child sprites into the main sprite list"""
        sprites.extend(self.child_sprites)
        self.child_sprites = []


class BallMob(Mob):
    def __init__(self, pos, direction="right"):
        Mob.__init__(self, "car", pos, direction)

        self.solid = True
        self.mu_coefficient = 1.0

        # image rotation-related
        self.image_real = self.image
        self.image_heading = 0

        self.spawn()

    def spawn(self):
        """Reset our variables to default"""
        self.dead = False
        self.set_move_delta(0, 0)

        # keep our degree heading separate, as the sprite can go upside down
        self.heading = 0
        self.spin = 0.0

        # animation
        self.start_anim(self.anim_move())

    def activate(self, direction):
        """Put this ball into play"""
        theta = random.random() * 1.2 - 0.5
        theta = random.choice((theta, theta + math.pi))
        dr = random.random() * 4.0 + 7

        self.set_move_vector(theta, dr)
        self.spin = random.random() * 14.0 - 7

    def do_sprite_rotate(self, force = False):
        """Rotate the canonical image to produce one suitable for our heading

        Returns True if the image changed

        Called from the animation coroutines
        """
        if (not force) and abs(self.heading - self.image_heading) < 0.5:
            # ignore small changes since this is quite an expensive operation
            return False

        tmp_image = self.image_real

        self.image = pygame.transform.rotate(tmp_image, self.heading)
        self.image_heading = self.heading

        # maintain the center of our rect
        center = self.rect.center
        self.rect.size = self.image.get_size()
        self.rect.center = center

        return True

    def anim_move(self):
        """Animation coroutine for move animation"""
        frames = self.get_frames("idle")
        force_rotate = True
        image_changed = True

        while True:
            for frame in frames:
                for i in range(15):
                    if (i == 0) and (frame != self.image_real):
                        self.image = frame
                        self.image_real = frame
                        force_rotate = True

                    image_changed = self.do_sprite_rotate(force=force_rotate)
                    force_rotate = False

                    yield image_changed


class TestMob(Mob):
    def __init__(self, pos, direction="none"):
        Mob.__init__(self, "bubbles", pos, direction)
        self.start_anim(self.anim_walk())

    def anim_stand(self):
        """Animation coroutine, start walking again"""
        frames = self.get_frames("stand")

        old_speed = (self.dx, self.dy)
        self.dx = self.dy = 0

        duration = random.randint(120,360)

        for frame in frames:
            self.image = frame
            yield True

        for delay in xrange(duration):
            yield False

        self.dx, self.dy = old_speed
        yield self.start_anim(self.anim_walk())

    def anim_walk(self):
        """Animation coroutine, randomly stop."""
        frames = self.get_frames("walk")
        
        for i in range(random.randint(10, 50)):
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(12):
                    yield False

        yield self.start_anim(self.anim_stand())


if __name__ == "__main__":
    import game
    game.main()
