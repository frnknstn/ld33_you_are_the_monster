#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import pygame.font as pygame_font_module      # avoid polluting other module's namespaces when they import *
from pygame.rect import Rect

DEBUG = False
#DEBUG = True

BENCHMARK = False

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")
SPRITE_DIR = "sprites"
CAP_FPS = None
#CAP_FPS = 20
CAP_FPS = 60

DOUBLE_SIZE = False
#DOUBLE_SIZE = True

GAME_STATES = {"config", "main"}

DRAW_MAP = True
#DRAW_MAP = False

# physics
AIR_RESISTANCE = 0.033
RESISTANCE_SCALE = 2

MONKEY_MAX_DY = 6
MONKEY_ACCELERATION = 0.3

BALL_MAX_THETA = 1.0

# interface layout
SCREEN_SIZE = (1600, 900)

TOP_EDGE = 130
BOTTOM_EDGE = 770
LEFT_EDGE = 0
RIGHT_EDGE = 1600

LEFT_BASELINE = 200
RIGHT_BASELINE = 1400
MIDLINE = (800, 450)

LEFT_SCORE_POS = (1450, 100)
RIGHT_SCORE_POS = (50, 100)

SCORE_DELAY = 90

TILE_SIZE = 32

DIRECTIONS = ("none", "up", "right", "down", "left")

DEFAULT_TEXTBLOCK_FONT = "fonts/bazar.ttf"

# game constants

# functions
def debug(*args):
    if DEBUG:
        print(*args)

def pygame_default_font(size):
    """Emulate the output of pygame.font.SysFont("", size), but in a way that doesn't crash if run through cx_Freeze"""
    
    # reduce the size to match what pygame's font.c:font_init() does
    # reportedly, this is to keep the modern pygame default font roughly the same size as that of older pygame versions.
    size = int(size * 0.6875)
    if size < 1:
        size = 1
    
    return pygame_font_module.Font("fonts/freesansbold.ttf", size)


if __name__ == "__main__":
    import game
    game.main()
